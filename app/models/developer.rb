class Developer < ApplicationRecord
  validates :email, uniqueness: true
  validates :email, uniqueness: true
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i }

  has_many :dev_pro_languages
  has_many :programming_languages, through: :dev_pro_languages
  has_many :dev_languages
  has_many :languages, through: :dev_languages

  def self.search_by_programming_languages(pro_lan)
    if pro_lan.nil? || pro_lan == ""
      self.all
    else
      search_key = pro_lan.split(" ")
      where('`programming_languages`.`name` IN (?) ', search_key)
    end
  end

  def self.search_by_languages(lan)
    if lan.nil? || lan == ""
      self.all
    else
      search_key = lan.split(" ")
      where('`languages`.`code` IN (?) ', search_key)
    end
  end
end
