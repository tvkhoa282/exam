class Language < ApplicationRecord
  validates :code, presence: true

  has_many :dev_languages
  has_many :developers, through: :dev_languages
end
