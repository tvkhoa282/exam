class ProgrammingLanguage < ApplicationRecord
  validates :name, presence: true
  
  has_many :dev_pro_languages
  has_many :developers, through: :dev_pro_languages
end
