class DevelopersController < ApplicationController
  def index
    developers_search = Developer.includes(:programming_languages)
                                .includes(:languages)
                                .search_by_programming_languages(params[:programming_languages])
                                .search_by_languages(params[:languages])
                                .pluck(:id).uniq

    @developers = Developer.includes(:programming_languages).includes(:languages).where(id: developers_search)
  end
end
