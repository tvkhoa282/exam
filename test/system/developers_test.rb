require "application_system_test_case"

class DevelopersTest < ApplicationSystemTestCase
  # test "visiting the index" do
  #   visit developers_url
  
  #   assert_selector "h1", text: "Developers"
  # end

  test "Search developers" do
    #visit developers page
    visit developers_url

    fill_in "programming languages",	with: ""
    fill_in "languages",	with: "en"
    click_button "Search"  

    assert_selector('table tbody tr', count: 2)
    all('table tbody tr > td:nth-child(3)').each do |td|
      assert td.has_text?('en')
    end
  end
end