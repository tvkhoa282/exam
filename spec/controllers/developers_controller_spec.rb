require 'rails_helper'

RSpec.describe DevelopersController, type: :controller do
  let!(:dev1) { create(:dev1) }
  let!(:dev2) { create(:dev2) }
  let!(:dev3) { create(:dev3) }

  let!(:ruby) { create(:ruby) }
  let!(:python) { create(:python) }
  let!(:php) { create(:php) }

  let!(:en) { create(:en) }
  let!(:jp) { create(:jp) }
  let!(:vi) { create(:vi) }

  let!(:dev1_ruby) { DevProLanguage.create(developer_id: dev1.id, programming_language_id: ruby.id) }
  let!(:dev1_python) { DevProLanguage.create(developer_id: dev1.id, programming_language_id: python.id) }
  let!(:dev1_en) { DevLanguage.create(developer_id: dev1.id, language_id: en.id) }

  let!(:dev2_ruby) { DevProLanguage.create(developer_id: dev2.id, programming_language_id: ruby.id) }
  let!(:dev2_php) { DevProLanguage.create(developer_id: dev2.id, programming_language_id: php.id) }
  let!(:dev2_en) { DevLanguage.create(developer_id: dev2.id, language_id: en.id) }
  let!(:dev2_jp) { DevLanguage.create(developer_id: dev2.id, language_id: jp.id) }

  let!(:dev3_ruby) { DevProLanguage.create(developer_id: dev3.id, programming_language_id: ruby.id) }
  let!(:dev3_php) { DevProLanguage.create(developer_id: dev3.id, programming_language_id: php.id) }
  let!(:dev3_vi) { DevLanguage.create(developer_id: dev3.id, language_id: vi.id) }

  describe "Search function case 1" do

    before do
      get :index, params: { programming_languages: "ruby python", languages: "vi" }
    end

    it 'return status code 200' do
      expect(response).to render_template("index")
    end

    it 'should return dev 3' do
      expect(assigns(:developers).pluck(:id).uniq).to eq([3])
    end
  end

  describe "Search function case 2" do

    before do
      get :index, params: { programming_languages: "php" }
    end

    it 'return status code 200' do
      expect(response).to render_template("index")
    end

    it 'should return dev 2, dev 3' do
      expect(assigns(:developers).pluck(:id).uniq).to eq([2, 3])
    end
  end

  describe "Search function case 3" do

    before do
      get :index, params: { languages: "en" }
    end

    it 'return status code 200' do
      expect(response).to render_template("index")
    end

    it 'should return dev1, dev2' do
      expect(assigns(:developers).pluck(:id).uniq).to eq([1, 2])
    end
  end

  describe "Search function case 4" do

    before do
      get :index, params: { programming_languages: "php", languages: "en" }
    end

    it 'return status code 200' do
      expect(response).to render_template("index")
    end

    it 'should return dev2' do
      expect(assigns(:developers).pluck(:id).uniq).to eq([2])
    end
  end

  describe "Search function case 5" do

    before do
      get :index, params: { programming_languages: "python", languages: "jp" }
    end

    it 'return status code 200' do
      expect(response).to render_template("index")
    end

    it 'should return no dev' do
      expect(assigns(:developers).pluck(:id).uniq).to eq([])
    end
  end
end
