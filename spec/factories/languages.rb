FactoryGirl.define do
  factory :en, class: Language do
    code "en"
  end
  
  factory :vi, class: Language do
    code "vi"
  end

  factory :jp, class: Language do
    code "jp"
  end
end