FactoryGirl.define do
  factory :dev1, class: Developer do
    email "dev1@gmail.com"
  end

  factory :dev2, class: Developer do
    email "dev2@gmail.com"
  end

  factory :dev3, class: Developer do
    email "dev3@gmail.com"
  end
end