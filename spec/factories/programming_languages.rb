FactoryGirl.define do
  factory :ruby, class: ProgrammingLanguage do
    name "ruby"
  end
  
  factory :python, class: ProgrammingLanguage do
    name "python"
  end

  factory :php, class: ProgrammingLanguage do
    name "php"
  end
end