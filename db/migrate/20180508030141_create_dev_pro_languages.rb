class CreateDevProLanguages < ActiveRecord::Migration[5.1]
  def change
    create_table :dev_pro_languages do |t|
      t.references :developer, foreign_key: true
      t.references :programming_language, foreign_key: true

      t.timestamps
    end
  end
end
